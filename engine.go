package main

import (
	"fmt"
	"os"
	"image/gif"
	"github.com/drgrib/iter"
	"image"
	//"image/png"
	"image/draw"
	"image/jpeg"
	"github.com/nfnt/resize"
	"github.com/disintegration/imaging"
	"image/color"
	"encoding/base64"
	"strings"
	"bytes"
	"image/png"
)

type Pos struct {
	DeltaX int `json:"deltaX"`
	DeltaY int `json:"deltaY"`
	LastX int `json:"lastX"`
	LastY int `json:"lastY"`
	X int `json:"x"`
	Y int `json:"y"`
	Angle int `json:"angle"`
}


type Frame struct {
	Data string `json:"data"`
}

type Item struct {
	Id       int `json:"id"`
	Path     string `json:"path"`
	Uuid     string  `json:"uuid"`
	Pos      Pos
	Width    int `json:"width"`
	Height   int `json:"height"`
	ZIndex   int `json:"zIndex"`
    Ext      string  `json:"ext"`
	GifFrame int `json:"gifFrame"`
	Frames []Frame
//	Decoded  *gif.GIF `json:"decoded"`
}

type ItemLink struct {
	Description string `json:"description"`
	Path string  `json:"path"`
	Title string  `json:"title"`
}

type Data struct {
	Width int `json:"width"`
	Height int `json:"height"`
	Items []Item
	ItemLink ItemLink
}

func main() {
	fmt.Println("start")

	var pos_1 Pos
	pos_1.X = 10
	pos_1.Y = 10
	pos_1.Angle = 0

	var pos_2 Pos
	pos_2.X = -100
	pos_2.Y = -100
	pos_2.Angle = 0

	var it_1 Item
	it_1.Id = 1
	it_1.Width = 650
	it_1.Height = 350
	it_1.Path = "image/base.jpg"
	it_1.Ext = "jpeg"
	it_1.Pos = pos_1
	it_1.ZIndex = 10

	var it_2 Item
	it_2.Id = 2
	it_2.Width = 161
	it_2.Height = 161
	it_2.Path = "image/gif1.gif"
	it_2.Ext = "gif"
	it_2.Pos = pos_2
	it_2.ZIndex = 20

	var it_3 Item
	it_3.Id = 3
	it_3.Width = 200
	it_3.Height = 200
	it_3.Path = "image/gif2.gif"
	it_3.Ext = "gif"
	it_3.Pos = pos_1
	it_3.ZIndex = 30

	var frm_1 Frame
	frm_1.Data = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAooAAAFuCAYAAAAGSTmFAAAVzUlEQVR4Xu3YsW0dQRADUF8/7r8S9XMOnShYfUCcIfY5Xml4jwoIP3/8I0CAAAECBAgQIPCNwEOFAAECBAgQIECAwHcChqK/CwIECBAgQIAAgW8FDEV/GAQIECBAgAABAoaivwECBAgQIECAAIFzAf+jeG7lJQECBAgQIEDgKgFD8aq6fSwBAgQIECBA4FzAUDy38pIAAQIECBAgcJWAoXhV3T6WAAECBAgQIHAuYCieW3lJgAABAgQIELhKwFC8qm4fS4AAAQIECBA4FzAUz628JECAAAECBAhcJWAoXlW3jyVAgAABAgQInAsYiudWXhIgQIAAAQIErhIwFK+q28cSIECAAAECBM4FDMVzKy8JECBAgAABAlcJGIpX1e1jCRAgQIAAAQLnAobiuZWXBAgQIECAAIGrBAzFq+r2sQQIECBAgACBcwFD8dzKSwIECBAgQIDAVQKG4lV1+1gCBAgQIECAwLmAoXhu5SUBAgQIECBA4CoBQ/Gqun0sAQIECBAgQOBcwFA8t/KSAAECBAgQIHCVgKF4Vd0+lgABAgQIECBwLmAonlt5SYAAAQIECBC4SsBQvKpuH0uAAAECBAgQOBcwFM+tvCRAgAABAgQIXCVgKF5Vt48lQIAAAQIECJwLGIrnVl4SIECAAAECBK4SMBSvqtvHEiBAgAABAgTOBQzFcysvCRAgQIAAAQJXCRiKV9XtYwkQIECAAAEC5wKG4rmVlwQIECBAgACBqwQMxavq9rEECBAgQIAAgXOB46H4vu/X+a/9//J5nr+f/JyfIUCAAAECBAgQmBX4yVB8P4n6PM/xjU9+v58hQIAAAQIECBD4HYHjEfe+r6H4Ox34rQQIECBAgACBlQKG4spahCJAgAABAgQIzAsYivMdSECAAAECBAgQWClgKK6sRSgCBAgQIECAwLyAoTjfgQQECBAgQIAAgZUChuLKWoQiQIAAAQIECMwLGIrzHUhAgAABAgQIEFgpYCiurEUoAgQIECBAgMC8gKE434EEBAgQIECAAIGVAobiylqEIkCAAAECBAjMCxiK8x1IQIAAAQIECBBYKWAorqxFKAIECBAgQIDAvIChON+BBAQIECBAgACBlQKG4spahCJAgAABAgQIzAsYivMdSECAAAECBAgQWClgKK6sRSgCBAgQIECAwLyAoTjfgQQECBAgQIAAgZUChuLKWoQiQIAAAQIECMwLGIrzHUhAgAABAgQIEFgpYCiurEUoAgQIECBAgMC8gKE434EEBAgQIECAAIGVAobiylqEIkCAAAECBAjMCxiK8x1IQIAAAQIECBBYKWAorqxFKAIECBAgQIDAvIChON+BBAQIECBAgACBlQKG4spahCJAgAABAgQIzAsYivMdSECAAAECBAgQWClgKK6sRSgCBAgQIECAwLyAoTjfgQQECBAgQIAAgZUChuLKWoQiQIAAAQIECMwLGIrzHUhAgAABAgQIEFgpYCiurEUoAgQIECBAgMC8gKE434EEBAgQIECAAIGVAobiylqEIkCAAAECBAjMCxiK8x1IQIAAAQIECBBYKWAorqxFKAIECBAgQIDAvIChON+BBAQIECBAgACBlQKG4spahCJAgAABAgQIzAsYivMdSECAAAECBAgQWClgKK6sRSgCBAgQIECAwLyAoTjfgQQECBAgQIAAgZUChuLKWoQiQIAAAQIECMwLGIrzHUhAgAABAgQIEFgpYCiurEUoAgQIECBAgMC8gKE434EEBAgQIECAAIGVAobiylqEIkCAAAECBAjMCxiK8x1IQIAAAQIECBBYKWAorqxFKAIECBAgQIDAvIChON+BBAQIECBAgACBlQKG4spahCJAgAABAgQIzAsYivMdSECAAAECBAgQWClgKK6sRSgCBAgQIECAwLyAoTjfgQQECBAgQIAAgZUChuLKWoQiQIAAAQIECMwLGIrzHUhAgAABAgQIEFgpYCiurEUoAgQIECBAgMC8gKE434EEBAgQIECAAIGVAobiylqEIkCAAAECBAjMCxiK8x1IQIAAAQIECBBYKWAorqxFKAIECBAgQIDAvIChON+BBAQIECBAgACBlQKG4spahCJAgAABAgQIzAsYivMdSECAAAECBAgQWClgKK6sRSgCBAgQIECAwLyAoTjfgQQECBAgQIAAgZUChuLKWoQiQIAAAQIECMwLGIrzHUhAgAABAgQIEFgpYCiurEUoAgQIECBAgMC8gKE434EEBAgQIECAAIGVAobiylqEIkCAAAECBAjMCxiK8x1IQIAAAQIECBBYKWAorqxFKAIECBAgQIDAvIChON+BBAQIECBAgACBlQKG4spahCJAgAABAgQIzAsYivMdSECAAAECBAgQWClgKK6sRSgCBAgQIECAwLyAoTjfgQQECBAgQIAAgZUChuLKWoQiQIAAAQIECMwLGIrzHUhAgAABAgQIEFgpYCiurEUoAgQIECBAgMC8gKE434EEBAgQIECAAIGVAobiylqEIkCAAAECBAjMCxiK8x1IQIAAAQIECBBYKWAorqxFKAIECBAgQIDAvIChON+BBAQIECBAgACBlQKG4spahCJAgAABAgQIzAsYivMdSECAAAECBAgQWClgKK6sRSgCBAgQIECAwLyAoTjfgQQECBAgQIAAgZUChuLKWoQiQIAAAQIECMwLGIrzHUhAgAABAgQIEFgpYCiurEUoAgQIECBAgMC8gKE434EEBAgQIECAAIGVAobiylqEIkCAAAECBAjMCxiK8x1IQIAAAQIECBBYKWAorqxFKAIECBAgQIDAvIChON+BBAQIECBAgACBlQKG4spahCJAgAABAgQIzAv8ZCh+fRL3eZ6/n/ycnyFAgAABAgQIEJgVOB6KszFdJ0CAAAECBAgQSAsYimlx9wgQIECAAAECJQKGYklRYhIgQIAAAQIE0gKGYlrcPQIECBAgQIBAiYChWFKUmAQIECBAgACBtIChmBZ3jwABAgQIECBQImAolhQlJgECBAgQIEAgLWAopsXdI0CAAAECBAiUCBiKJUWJSYAAAQIECBBICxiKaXH3CBAgQIAAAQIlAoZiSVFiEiBAgAABAgTSAoZiWtw9AgQIECBAgECJgKFYUpSYBAgQIECAAIG0gKGYFnePAAECBAgQIFAiYCiWFCUmAQIECBAgQCAtYCimxd0jQIAAAQIECJQIGIolRYlJgAABAgQIEEgLGIppcfcIECBAgAABAiUChmJJUWISIECAAAECBNIChmJa3D0CBAgQIECAQImAoVhSlJgECBAgQIAAgbSAoZgWd48AAQIECBAgUCJgKJYUJSYBAgQIECBAIC1gKKbF3SNAgAABAgQIlAgYiiVFiUmAAAECBAgQSAsYimlx9wgQIECAAAECJQKGYklRYhIgQIAAAQIE0gKGYlrcPQIECBAgQIBAiYChWFKUmAQIECBAgACBtIChmBZ3jwABAgQIECBQImAolhQlJgECBAgQIEAgLWAopsXdI0CAAAECBAiUCBiKJUWJSYAAAQIECBBICxiKaXH3CBAgQIAAAQIlAoZiSVFiEiBAgAABAgTSAoZiWtw9AgQIECBAgECJgKFYUpSYBAgQIECAAIG0gKGYFnePAAECBAgQIFAiYCiWFCUmAQIECBAgQCAtYCimxd0jQIAAAQIECJQIGIolRYlJgAABAgQIEEgLGIppcfcIECBAgAABAiUChmJJUWISIECAAAECBNIChmJa3D0CBAgQIECAQImAoVhSlJgECBAgQIAAgbSAoZgWd48AAQIECBAgUCJgKJYUJSYBAgQIECBAIC1gKKbF3SNAgAABAgQIlAgYiiVFiUmAAAECBAgQSAsYimlx9wgQIECAAAECJQKGYklRYhIgQIAAAQIE0gKGYlrcPQIECBAgQIBAiYChWFKUmAQIECBAgACBtIChmBZ3jwABAgQIECBQImAolhQlJgECBAgQIEAgLWAopsXdI0CAAAECBAiUCBiKJUWJSYAAAQIECBBICxiKaXH3CBAgQIAAAQIlAoZiSVFiEiBAgAABAgTSAoZiWtw9AgQIECBAgECJgKFYUpSYBAgQIECAAIG0gKGYFnePAAECBAgQIFAiYCiWFCUmAQIECBAgQCAtYCimxd0jQIAAAQIECJQIGIolRYlJgAABAgQIEEgLGIppcfcIECBAgAABAiUChmJJUWISIECAAAECBNIChmJa3D0CBAgQIECAQImAoVhSlJgECBAgQIAAgbSAoZgWd48AAQIECBAgUCJgKJYUJSYBAgQIECBAIC1gKKbF3SNAgAABAgQIlAgYiiVFiUmAAAECBAgQSAsYimlx9wgQIECAAAECJQKGYklRYhIgQIAAAQIE0gKGYlrcPQIECBAgQIBAiYChWFKUmAQIECBAgACBtIChmBZ3jwABAgQIECBQImAolhQlJgECBAgQIEAgLWAopsXdI0CAAAECBAiUCBiKJUWJSYAAAQIECBBICxiKaXH3CBAgQIAAAQIlAoZiSVFiEiBAgAABAgTSAoZiWtw9AgQIECBAgECJgKFYUpSYBAgQIECAAIG0gKGYFnePAAECBAgQIFAiYCiWFCUmAQIECBAgQCAtYCimxd0jQIAAAQIECJQIGIolRYlJgAABAgQIEEgLGIppcfcIECBAgAABAiUChmJJUWISIECAAAECBNIChmJa3D0CBAgQIECAQImAoVhSlJgECBAgQIAAgbSAoZgWd48AAQIECBAgUCJgKJYUJSYBAgQIECBAIC1gKKbF3SNAgAABAgQIlAgYiiVFiUmAAAECBAgQSAsYimlx9wgQIECAAAECJQKGYklRYhIgQIAAAQIE0gKGYlrcPQIECBAgQIBAiYChWFKUmAQIECBAgACBtIChmBZ3jwABAgQIECBQImAolhQlJgECBAgQIEAgLWAopsXdI0CAAAECBAiUCBiKJUWJSYAAAQIECBBICxiKaXH3CBAgQIAAAQIlAoZiSVFiEiBAgAABAgTSAoZiWtw9AgQIECBAgECJgKFYUpSYBAgQIECAAIG0gKGYFnePAAECBAgQIFAiYCiWFCUmAQIECBAgQCAtYCimxd0jQIAAAQIECJQIGIolRYlJgAABAgQIEEgLGIppcfcIECBAgAABAiUChmJJUWISIECAAAECBNIChmJa3D0CBAgQIECAQImAoVhSlJgECBAgQIAAgbSAoZgWd48AAQIECBAgUCJgKJYUJSYBAgQIECBAIC1gKKbF3SNAgAABAgQIlAgYiiVFiUmAAAECBAgQSAsYimlx9wgQIECAAAECJQKGYklRYhIgQIAAAQIE0gKGYlrcPQIECBAgQIBAiYChWFKUmAQIECBAgACBtIChmBZ3jwABAgQIECBQImAolhQlJgECBAgQIEAgLWAopsXdI0CAAAECBAiUCBiKJUWJSYAAAQIECBBICxiKaXH3CBAgQIAAAQIlAoZiSVFiEiBAgAABAgTSAoZiWtw9AgQIECBAgECJgKFYUpSYBAgQIECAAIG0gKGYFnePAAECBAgQIFAiYCiWFCUmAQIECBAgQCAtYCimxd0jQIAAAQIECJQIGIolRYlJgAABAgQIEEgLGIppcfcIECBAgAABAiUChmJJUWISIECAAAECBNIChmJa3D0CBAgQIECAQImAoVhSlJgECBAgQIAAgbSAoZgWd48AAQIECBAgUCJgKJYUJSYBAgQIECBAIC1gKKbF3SNAgAABAgQIlAgYiiVFiUmAAAECBAgQSAsYimlx9wgQIECAAAECJQKGYklRYhIgQIAAAQIE0gKGYlrcPQIECBAgQIBAiYChWFKUmAQIECBAgACBtIChmBZ3jwABAgQIECBQImAolhQlJgECBAgQIEAgLWAopsXdI0CAAAECBAiUCBiKJUWJSYAAAQIECBBICxiKaXH3CBAgQIAAAQIlAoZiSVFiEiBAgAABAgTSAoZiWtw9AgQIECBAgECJgKFYUpSYBAgQIECAAIG0gKGYFnePAAECBAgQIFAiYCiWFCUmAQIECBAgQCAtYCimxd0jQIAAAQIECJQIGIolRYlJgAABAgQIEEgLGIppcfcIECBAgAABAiUChmJJUWISIECAAAECBNIChmJa3D0CBAgQIECAQImAoVhSlJgECBAgQIAAgbSAoZgWd48AAQIECBAgUCJgKJYUJSYBAgQIECBAIC1gKKbF3SNAgAABAgQIlAgYiiVFiUmAAAECBAgQSAsYimlx9wgQIECAAAECJQKGYklRYhIgQIAAAQIE0gKGYlrcPQIECBAgQIBAiYChWFKUmAQIECBAgACBtIChmBZ3jwABAgQIECBQImAolhQlJgECBAgQIEAgLWAopsXdI0CAAAECBAiUCBiKJUWJSYAAAQIECBBICxiKaXH3CBAgQIAAAQIlAoZiSVFiEiBAgAABAgTSAoZiWtw9AgQIECBAgECJgKFYUpSYBAgQIECAAIG0gKGYFnePAAECBAgQIFAiYCiWFCUmAQIECBAgQCAtYCimxd0jQIAAAQIECJQIGIolRYlJgAABAgQIEEgLGIppcfcIECBAgAABAiUChmJJUWISIECAAAECBNIChmJa3D0CBAgQIECAQImAoVhSlJgECBAgQIAAgbSAoZgWd48AAQIECBAgUCJgKJYUJSYBAgQIECBAIC1gKKbF3SNAgAABAgQIlAgYiiVFiUmAAAECBAgQSAsYimlx9wgQIECAAAECJQKGYklRYhIgQIAAAQIE0gKGYlrcPQIECBAgQIBAiYChWFKUmAQIECBAgACBtIChmBZ3jwABAgQIECBQImAolhQlJgECBAgQIEAgLWAopsXdI0CAAAECBAiUCBiKJUWJSYAAAQIECBBICxiKaXH3CBAgQIAAAQIlAoZiSVFiEiBAgAABAgTSAoZiWtw9AgQIECBAgECJgKFYUpSYBAgQIECAAIG0gKGYFnePAAECBAgQIFAiYCiWFCUmAQIECBAgQCAtYCimxd0jQIAAAQIECJQIGIolRYlJgAABAgQIEEgLGIppcfcIECBAgAABAiUChmJJUWISIECAAAECBNIChmJa3D0CBAgQIECAQImAoVhSlJgECBAgQIAAgbSAoZgWd48AAQIECBAgUCJgKJYUJSYBAgQIECBAIC1gKKbF3SNAgAABAgQIlAgYiiVFiUmAAAECBAgQSAsYimlx9wgQIECAAAECJQKGYklRYhIgQIAAAQIE0gKGYlrcPQIECBAgQIBAiYChWFKUmAQIECBAgACBtIChmBZ3jwABAgQIECBQImAolhQlJgECBAgQIEAgLWAopsXdI0CAAAECBAiUCBiKJUWJSYAAAQIECBBICxiKaXH3CBAgQIAAAQIlAoZiSVFiEiBAgAABAgTSAoZiWtw9AgQIECBAgECJgKFYUpSYBAgQIECAAIG0gKGYFnePAAECBAgQIFAi8A/DhZV+SmJC+AAAAABJRU5ErkJggg=="

	var it_4 Item
	it_4.Id = 4
	it_4.Width = 200
	it_4.Height = 200
	it_4.Path = ""
	it_4.Ext = "base64png"
	it_4.Pos = pos_1
	it_4.ZIndex = 41
	it_4.Frames = []Frame{frm_1}


	var data Data
	data.Width = 650
	data.Height = 350
	data.Items = []Item{it_1,it_3,it_2, it_4}

	var max = 0

	fmt.Println(data)
	//fmt.Println(it_4)
	//fmt.Println(pos_1)

	// определеляем сколкько фреймов в гифках
	for i, item := range data.Items {
		fmt.Println(i, item)
		if item.Ext == "gif" {
			fmt.Println("yes")
			file, err := os.Open(item.Path)
			if err != nil {
				fmt.Print(err)
			}
			img, err := gif.DecodeAll(file)
			if err != nil {
				fmt.Print(err)
			}
			imgLen := len(img.Image)//int(len(img.Image)/2)
			if max <= imgLen {
				max = imgLen
			}
			item.GifFrame = imgLen
			//item.Decoded = img
			fmt.Println(item)
			//fmt.Println(reflect.TypeOf(img))
		}
		if item.Ext == "base64png"{

			for n, fr := range item.Frames {
				unbased, err := base64.StdEncoding.DecodeString(strings.Replace(fr.Data,
					"data:image/png;base64,", "", 1))
				if err != nil {
					fmt.Println("Cannot decode b64")
				}
				fmt.Println("base64", n)
				fmt.Println(unbased)
				r := bytes.NewReader(unbased)
				im, err := png.Decode(r)
				if err != nil {
					panic("Bad png")
				}

				f, err := os.OpenFile(fmt.Sprintf("gf/%d.jpeg", item.Id), os.O_WRONLY|os.O_CREATE, 0777)
				if err != nil {
					panic("Cannot open file")
				}

				png.Encode(f, im)
			}

		}
	}

	fmt.Println("max", max)
	return
	items := data.Items

	for m := range iter.N(max) {
		base_image := items[0]
		fmt.Println(m, base_image)

		im := image.NewRGBA(image.Rect(0, 0, data.Width, data.Height))
		//fmt.Println(im)
		for z, it := range data.Items {
			fmt.Println(z, it)
			if it.Ext == "gif" {
				file, err := os.Open(it.Path)
				if err != nil {
					fmt.Print(err)
				}
				gif_img, err := gif.DecodeAll(file)
				if err != nil {
					fmt.Print(err)
				}

				fmt.Println(float32(len(gif_img.Image)-m))

				if m < len(gif_img.Image) {
					g_m := resize.Resize(uint(it.Width), 0, gif_img.Image[m], resize.Lanczos3)
					if it.Pos.Angle > 0 {
						g_m = imaging.Rotate(g_m, float64(it.Pos.Angle), color.RGBA{})
					}
					draw.Draw(im, im.Bounds(), g_m, image.Point{it.Pos.X, it.Pos.Y}, draw.Over)
				}
			}
			if it.Ext == "jpeg" {
				file, err := os.Open(it.Path)
				if err != nil {
					fmt.Print(err)
				}
				j_img, err := jpeg.Decode(file)
				if err != nil {
					fmt.Print(err)
				}
				j_m := resize.Resize(uint(it.Width), 0, j_img, resize.Lanczos3)
				draw.Draw(im, j_img.Bounds(), j_m, image.Point{it.Pos.X,it.Pos.Y}, draw.Over)
			}

		}

		img, _ := os.Create(fmt.Sprintf("dfr/%d.jpeg", m))

		jpeg.Encode(img, im, &jpeg.Options{jpeg.DefaultQuality})

		img.Close()

	}
}
