package main

import (
	"io/ioutil"
	"fmt"
	"github.com/icza/mjpeg"
)

func main() {
	checkErr := func(err error) {
		if err != nil {
			panic(err)
		}
	}

	// Video size: 200x100 pixels, FPS: 2
	aw, err := mjpeg.New("test.avi", 200, 100, 60)
	checkErr(err)

	// Create a movie from images: 1.jpg, 2.jpg, ..., 10.jpg
	for i := 1; i <= 10; i++ {
		data, err := ioutil.ReadFile(fmt.Sprintf("dfr/%d.png", i))
		checkErr(err)
		checkErr(aw.AddFrame(data))
	}

	checkErr(aw.Close())
}
