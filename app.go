package main

import (
	"fmt"
	"log"
	"github.com/PuerkitoBio/goquery"
	"strings"
	"os"
	"net/http"
	_ "io"
	"path/filepath"
	"time"
	"crypto/rand"
	"strconv"
	"io/ioutil"
	_ "github.com/valyala/fasthttp"
	"flag"
	"github.com/valyala/fasthttp"
	"encoding/json"
)

func download_file(download_link string) (string, error) {
	ext := filepath.Ext(download_link)
	if(strings.HasPrefix(ext, ".png")){
		ext = ".png"
	}
	if(strings.HasPrefix(ext, ".gif")){
		ext = ".gif"
	}
	if(strings.HasPrefix(ext, ".jpg")){
		ext = ".jpg"
	}
	fmt.Println(ext)

	dir_path, err := dir_full_path()
	if err != nil {
		return "", err
	}
	file_name := rand_str(10) + ext
	file_path := dir_path + file_name

	os.Mkdir(dir_path, 0666)

	file, err := os.Create(file_path)
	if err != nil {
		return "", err
	}
	defer file.Close()

	res, err := http.Get(download_link)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	file_content, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return "", err
	}

	// returns file size and err
	_, err = file.Write(file_content)

	if err != nil {
		return "", err
	}

	return file_path, nil
}

func dir_full_path() (string, error) {
	path, err := filepath.Abs("C:\\my\\view_contter\\assets\\media") // "files"

	if err != nil {
		return "", err
	}

	t := time.Now()

	s := path +
		string(os.PathSeparator) +
		strconv.Itoa(t.Day()) +
		"_" +
		strconv.Itoa(int(t.Month())) +
		"_" +
		strconv.Itoa(t.Year()) +
		string(os.PathSeparator)

	return s, nil
}

func rand_str(n int) string {
	alphanum := "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

	var bytes = make([]byte, n)
	rand.Read(bytes)
	for i, b := range bytes {
		bytes[i] = alphanum[b%byte(len(alphanum))]
	}
	return string(bytes)
}

func ExampleScrape(link string) (string, string, string) {
	//https://techcrunch.com/2017/06/01/skypes-snapchat-inspired-makeover-puts-the-camera-a-swipe-away-adds-stories/
	statusCode, body, err2 := fasthttp.Get(nil, link)
	if err2 != nil {
		// handler error
	}
	fmt.Println(statusCode)
	/*utfBody, err := iconv.NewReader(strings.NewReader(string(body)), "", "utf-8")
	if err != nil {
		// handler error
	}*/

	// use utfBody using goquery
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(string(body)))
	if err != nil {
		log.Fatal(err)
	}
	pageTitle := ""
	description := ""
	path := ""
	// start scraping page title
	doc.Find("head").Each(func(i int, s *goquery.Selection) {
		pageTitle = s.Find("title").Text()
		fmt.Println(pageTitle)
	})
	// now get meta description field
	doc.Find("meta").Each(func(i int, s *goquery.Selection) {
		if name, _ := s.Attr("name"); strings.EqualFold(name, "description") {
			description, _ = s.Attr("content")
			fmt.Println(description)
		}
		if name, _ := s.Attr("property"); strings.EqualFold(name, "og:image") {
			image, _ := s.Attr("content")
			path, err = download_file(image)
			fmt.Println(image, path, err)
		}
	})
	// Find the review items
	doc.Find(".b-news-list .list__item").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		band := s.Find("a").Text()
		title := s.Find("i").Text()
		fmt.Printf("Review %d: %s - %s\n", i, band, title)
	})

	doc.Find("img").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		//band := s.Find("a").Text()
		//title := s.Find("i").Text()
		fmt.Println(s.Attr("src"))
	})

	//fmt.Println(string(body))


	return string(pageTitle), string(description), string(path)
}

var (
	addr     = flag.String("addr", ":8075", "TCP address to listen to")
	compress = flag.Bool("compress", false, "Whether to enable transparent response compression")
)


func main() {
	//ExampleScrape()
	flag.Parse()

	h := requestHandler
	if *compress {
		h = fasthttp.CompressHandler(h)
	}

	if err := fasthttp.ListenAndServe(*addr, h); err != nil {
		log.Fatalf("Error in ListenAndServe: %s", err)
	}
}

type Js struct {
	Link string `json:"link"`
}

func requestHandler(ctx *fasthttp.RequestCtx) {
	ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
	ctx.Response.Header.Set("Access-Control-Allow-Headers", "Authorization, Content-Type")


	if(len(string(ctx.PostBody())) == 0){
		return
	}
	//link := ctx.QueryArgs().Peek("link").
	//link := string(ctx.QueryArgs().Peek("link"))
	//link := string(ctx.PostArgs().Peek("link"))




	ctx.SetContentType("application/json; charset=utf-8")

	in := string(ctx.PostBody())

	rawIn := json.RawMessage(in)
	bytes, err := rawIn.MarshalJSON()
	if err != nil {
		panic(err)
	}

	var p Js
	err = json.Unmarshal(bytes, &p)
	if err != nil {
		panic(err)
	}

	title, description, path := ExampleScrape(p.Link)

	fmt.Fprintf(ctx, "{\"title\":%q, \"description\":%q, \"path\":%q}", title, description, path)


	/*fmt.Fprintf(ctx, "Hello, world!\n\n")

	fmt.Fprintf(ctx, "Request method is %q\n", ctx.Method())
	fmt.Fprintf(ctx, "RequestURI is %q\n", ctx.RequestURI())
	fmt.Fprintf(ctx, "Requested path is %q\n", ctx.Path())
	fmt.Fprintf(ctx, "Host is %q\n", ctx.Host())
	fmt.Fprintf(ctx, "Query string is %q\n", ctx.QueryArgs())
	fmt.Fprintf(ctx, "User-Agent is %q\n", ctx.UserAgent())
	fmt.Fprintf(ctx, "Connection has been established at %s\n", ctx.ConnTime())
	fmt.Fprintf(ctx, "Request has been started at %s\n", ctx.Time())
	fmt.Fprintf(ctx, "Serial request number for the current connection is %d\n", ctx.ConnRequestNum())
	fmt.Fprintf(ctx, "Your ip is %q\n\n", ctx.RemoteIP())

	fmt.Fprintf(ctx, "Raw request is:\n---CUT---\n%s\n---CUT---", &ctx.Request)

	ctx.SetContentType("text/plain; charset=utf8")

	// Set arbitrary headers
	ctx.Response.Header.Set("X-My-Header", "my-header-value")

	// Set cookies
	var c fasthttp.Cookie
	c.SetKey("cookie-name")
	c.SetValue("cookie-value")
	ctx.Response.Header.SetCookie(&c)*/
}