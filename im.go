package main

import (
	"os"
	"log"
	"image/gif"
	//"image"
	//"fmt"
	"image"
	"fmt"
	"image/png"
	"image/draw"
	"image/jpeg"
)
func main() {
	in, err := os.Open("gif.gif")
	if err != nil {
		log.Fatalln(err)
	}
	defer in.Close()

	decoded, err := gif.DecodeAll(in)
	if err != nil {
		log.Fatalln(err)
	}

	in2, err := os.Open("gman.gif")
	if err != nil {
		log.Fatalln(err)
	}
	defer in.Close()

	decoded2, err := gif.DecodeAll(in2)
	if err != nil {
		log.Fatalln(err)
	}

	fImg1, _ := os.Open("one.jpg")
	defer fImg1.Close()
	img1, _ := jpeg.Decode(fImg1)

	names := []string{}

	for i, frame := range decoded.Image {
		frameFile, err := os.OpenFile(fmt.Sprintf("gf/%d.png", i+1), os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0666)
		if err != nil {
			log.Fatal(err)
		}
		err = png.Encode(frameFile, frame)
		if err != nil {
			log.Fatal(err)
		}

		frm := decoded2.Image[i]

		m := image.NewRGBA(img1.Bounds())
		draw.Draw(m, m.Bounds(), img1, image.Point{0,0}, draw.Src)
		draw.Draw(m, m.Bounds(), frame, image.Point{0,30}, draw.Over)
		//draw.Draw(m, m.Bounds(), frame, image.Point{-200,30}, draw.Over)
		draw.Draw(m, m.Bounds(), frm, image.Point{-900,30}, draw.Over)

		toimg, _ := os.Create(fmt.Sprintf("dfr/%d.png", i+1))

		png.Encode(toimg, m)

		// Not using defer here because we're in a loop, not a function
		frameFile.Close()
		toimg.Close()

		names = append(names, fmt.Sprintf("dfr/%d.png", i+1))


	}

}
