package main

import (
	"fmt"
	"log"
	_ "io"
	_ "github.com/valyala/fasthttp"
	"flag"
	"github.com/valyala/fasthttp"
	"encoding/json"
	"path/filepath"
	"os"
	"image/gif"
	"image/png"
	"image"
	"image/draw"
	"image/jpeg"
        "github.com/nfnt/resize"
)

var (
	addr     = flag.String("addr", ":8075", "TCP address to listen to")
	compress = flag.Bool("compress", false, "Whether to enable transparent response compression")
)


func main() {
	//ExampleScrape()
	flag.Parse()

	h := requestServerHandler
	if *compress {
		h = fasthttp.CompressHandler(h)
	}

	if err := fasthttp.ListenAndServe(*addr, h); err != nil {
		log.Fatalf("Error in ListenAndServe: %s", err)
	}
}

type Pos struct {
	DeltaX int `json:"deltaX"`
	DeltaY int `json:"deltaY"`
	LastX int `json:"lastX"`
	LastY int `json:"lastY"`
	X int `json:"x"`
	Y int `json:"y"`
}

type Item struct {
	Id int `json:"id"`
	Path string `json:"path"`
	Uuid string  `json:"uuid"`
	Pos Pos
	Width int `json:"width"`
	Height int `json:"height"`
}

type ItemLink struct {
	Description string `json:"description"`
	Path string  `json:"path"`
	Title string  `json:"title"`
}

type Data struct {
	Width int `json:"width"`
	Height int `json:"height"`
	Items []Item
	ItemLink ItemLink
}

func requestServerHandler(ctx *fasthttp.RequestCtx) {
	ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
	ctx.Response.Header.Set("Access-Control-Allow-Headers", "Authorization, Content-Type")


	if(len(string(ctx.PostBody())) == 0){
		return
	}
	//link := ctx.QueryArgs().Peek("link").
	//link := string(ctx.QueryArgs().Peek("link"))
	//link := string(ctx.PostArgs().Peek("link"))




	ctx.SetContentType("application/json; charset=utf-8")

	in := string(ctx.PostBody())

	rawIn := json.RawMessage(in)
	bytes, err := rawIn.MarshalJSON()
	if err != nil {
		panic(err)
	}

	var p Data
	err = json.Unmarshal(bytes, &p)
	if err != nil {
		panic(err)
	}

	fmt.Println(p)
	fmt.Println(p.ItemLink.Path)

	handlerImage(p)


}

/*
обработка картинок
*/
func handlerImage(Data Data) {

	pathGif := filepath.Join("C:\\my\\view_contter\\", Data.Items[0].Path)
	path := filepath.Join("C:\\my\\view_contter\\", Data.ItemLink.Path)

	fmt.Println(pathGif)

	in, err := os.Open(pathGif)
	if err != nil {
		log.Fatalln(err)
	}
	defer in.Close()

	decoded, err := gif.DecodeAll(in)
	if err != nil {
		log.Fatalln(err)
	}

	fImg1, _ := os.Open(path)
	defer fImg1.Close()
	img1, _ := jpeg.Decode(fImg1)

	names := []string{}

	for i, frame := range decoded.Image {

		frameFile, err := os.OpenFile(fmt.Sprintf("gf/%d.png", i+1), os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0666)
		if err != nil {
			log.Fatal(err)
		}

		mf := resize.Resize(uint(Data.Items[0].Width), uint(Data.Items[0].Height), frame, resize.Lanczos3)

		err = png.Encode(frameFile, mf)
		if err != nil {
			log.Fatal(err)
		}


		m := image.NewRGBA(img1.Bounds())
		//rt := image.Rectangle{0,0,0,0}
		//m := image.NewRGBA(rt)
		fmt.Println(img1.Bounds().Size().X, img1.Bounds().Size().Y)

		newX := getNewX(Data.Items[0].Pos.X, Data.Width, img1.Bounds().Size().X)
		newY := getNewY(Data.Items[0].Pos.Y, Data.Height, img1.Bounds().Size().Y)

		fmt.Println(newX, newY)

		draw.Draw(m, m.Bounds(), img1, image.Point{0,0}, draw.Src)
		draw.Draw(m, m.Bounds(), mf, image.Point{-newX,-newY}, draw.Over)
		//draw.Draw(m, m.Bounds(), frame, image.Point{-200,30}, draw.Over)

		toimg, _ := os.Create(fmt.Sprintf("dfr/%d.png", i+1))

		png.Encode(toimg, m)

		// Not using defer here because we're in a loop, not a function
		frameFile.Close()
		toimg.Close()

		names = append(names, fmt.Sprintf("dfr/%d.png", i+1))


	}

}

func getNewX(x int, oldWidth int, newWidth int) int {
	return (x * newWidth) / oldWidth
}

func getNewY(y int, oldHeight int, newHeight int) int {
	return (y * newHeight) / oldHeight
}
/*
function getNewX(xVlaue, oldWidth, newWidth){
   return xVlaue * newWidth / oldWidth;
}
*/
