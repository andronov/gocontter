package main

import (
	"os"
	"log"
	"image/gif"
	//"image"
	//"fmt"
	"image"
	"fmt"
	"image/png"
	"image/draw"
	"image/jpeg"
	"image/color"
)

/*
import (
	"os"
	"image/draw"
	"image"
	"image/jpeg"
)

func main() {
	fImg1, _ := os.Open("one.jpg")
	defer fImg1.Close()
	img1, _, _ := image.Decode(fImg1)

	fImg2, _ := os.Open("gif.gif")
	defer fImg2.Close()
	img2, _, _ := image.Decode(fImg2)

	m := image.NewRGBA(image.Rect(0, 0, 800, 600))
	draw.Draw(m, m.Bounds(), img1, image.Point{0,0}, draw.Src)
	draw.Draw(m, m.Bounds(), img2, image.Point{-200,-200}, draw.Src)

	toimg, _ := os.Create("new.gif")
	defer toimg.Close()

	jpeg.Encode(toimg, m, &jpeg.Options{jpeg.DefaultQuality})
}*/
func main() {
	in, err := os.Open("gif.gif")
	if err != nil {
		log.Fatalln(err)
	}
	defer in.Close()

	outGif := &gif.GIF{}

	decoded, err := gif.DecodeAll(in)
	if err != nil {
		log.Fatalln(err)
	}

	fImg1, _ := os.Open("one.jpg")
	defer fImg1.Close()
	img1, _ := jpeg.Decode(fImg1)

	names := []string{}


	for i, frame := range decoded.Image {
		frameFile, err := os.OpenFile(fmt.Sprintf("gf/%d.png", i+1), os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0666)
		if err != nil {
			log.Fatal(err)
		}
		err = png.Encode(frameFile, frame)
		if err != nil {
			log.Fatal(err)
		}

		//img2, _, _ := image.Decode(frameFile)

		m := image.NewRGBA(img1.Bounds())
		draw.Draw(m, m.Bounds(), img1, image.Point{0,0}, draw.Src)
		draw.Draw(m, m.Bounds(), frame, image.Point{0,0}, draw.Over)

		toimg, _ := os.Create(fmt.Sprintf("dfr/%d.png", i+1))

		png.Encode(toimg, m)

		// Not using defer here because we're in a loop, not a function
		frameFile.Close()
		toimg.Close()

		names = append(names, fmt.Sprintf("dfr/%d.png", i+1))

		/*f, _ := os.Open(fmt.Sprintf("dfr/%d.png", i+1))
		inGif, _ := jpeg.Decode(f)*/

		/*img := image.NewRGBA(image.Rect(0, 0, 640, 480))

		c := color.RGBA{255, 53, 0, 255}
		draw.Draw(img, image.Rect(10, 0, 640, 640), &image.Uniform{c},
			image.ZP, draw.Src)*/
		/*v := image.NewRGBA(inGif.Bounds())
		draw.Draw(v, v.Bounds(), inGif, image.Point{0,0}, draw.Src)

		Formatgif(v, fmt.Sprintf("dfr/g_%d.gif", i+1))*/
		//f.Close()

		/*f, _ := os.Open(fmt.Sprintf("dfr/%d.png", i+1))
		inGif, err := jpeg.Decode(f)
		if err != nil {
			log.Fatal(err)
		}
		f.Close()

		Formatgif(inGif, fmt.Sprintf("dfr/g_%d.gif", i+1))*/
		//fmt.Println(inGif.Bounds())
		//palImg := image.NewPaletted(inGif.Bounds() in)
		//outGif.Image = append(outGif.Image, palImg)
		//outGif.Delay = append(outGif.Delay, 0)


	}


	for i, name := range  names{
		fmt.Println(name, i)
		f, _ := os.Open(name)
		inGif, _ := png.Decode(f)

		fmt.Println(inGif.Bounds())
		palImg := image.NewPaletted(inGif.Bounds(), color.Palette{})
		outGif.Image = append(outGif.Image, palImg)
		outGif.Delay = append(outGif.Delay, 0)

		/*f, _ := os.Open(name)
		inGif, _ := png.Decode(f)
		Formatgif(inGif, fmt.Sprintf("dfr/g_%d.gif", i+1))*/
	}

	/*for ii, frm := range decoded.Image {
		//fmt.Println(decoded.Image[x])
		frame := frm

		palImg := image.NewPaletted(frame.Bounds(), frame.Palette)
		fmt.Println(len(decoded.Image), ii)
		//decoded.Image[x] = frame//palImg
		outGif.Image = append(outGif.Image, palImg)
		outGif.Delay = append(outGif.Delay, 0)
	}*/

	f, _ := os.Create("out.gif")
	defer f.Close()
	fmt.Println(outGif)
	gif.EncodeAll(f, outGif)
}

func Formatgif(img image.Image, name string) {
	out, err := os.Create(name)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	//var opt gif.Options
	//	opt.NumColors = 256
	err = gif.Encode(out, img, nil)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	out.Close()

	fmt.Println("\n success... \n ")

}